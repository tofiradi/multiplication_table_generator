# README #

## Multiplication Table Generator ##

### What is this repository for? ###

This repo has a PHP code that generates a multiplication table of an arbitrary size.

The code can be executed both through browser and PHP CLI. This is achieved by having two separate logics for dealing 
with the calling entity.

This code has the following features addressed the following issues:
    
	- Generating a multiplication table of an arbitrary size.

    - Making the top and left rows bold or otherwise highlighted.

    - Making all the cells have an even size.

    - PSR coding standards compliance:  - http://www.php-fig.org/psr/psr-2/

    - Proper documentation.

    - W3C compliant HTML5 and CSS

    - HTML uses a standard form

    - Code written in PHP 7 with strict types declared.

    - Generating both HTML and command line output.
	
--------------------------------------------------------------------------------------------------------------------------

### How do I get set up? ###


#### Requirements: ####



This program needs the ANSICON software to be installed (in order to Generate Command Line Colors with PHP) as qouted 
from his author:

1. Download ANSICON from: https://github.com/adoxa/ansicon

2. Extract the proper files (Depending on if you have a 32 or 64 bit machine) to c:\ansicon\ (For example). 
   I have a 32 bit machine and hence I extracted the files from inside the x86 folder.

3. Open an elevated privilege command line prompt and go to c:\ansicon, and then type 'ansicon -i' without the quotes

4. Add c:\ansicon to your path environment variable

5. Done. You can now enjoy the colored output.


--------------------------------------------------------------------------------------------------------------------------

### In particular, the program has three pages: ###

- index.php: The first page to run and output the result to the user.

- ProcessTable.php: plays as the intermediate tier for getting 
                    the inputs from index.php, preparing and organising them, 
					and sending them to the BuildTable.php page for the 
					creation of the result table. 

- BuildTable.php: responsible for the actual rendering of the multiplication 
                  table.


#### Note: all the styling were done in the following path: ####
 
	  multiplication_table_generator/css/Styles.css

--------------------------------------------------------------------------------------------------------------------------

### Calling Procedures ###

#### Execution route 1: Through Browser #####


- When executed through web, the index pages launches and displays the main interface which prompt for user input 
  of the number of columns and rows for generating the table.

- Upon pressing the 'Generate' button, index pages submits the values onto the ProcessTable.php page.

- 'ProcessTable.php' fetches the values into its local variables and calls 'genertaeTable' function with the passed 
  variables.

- The definition of the 'generateTableHtml' function resides in the 'BuildTable.php' page which now takes the control.

- 'generateTable' function firstly creates a string variable '$render_table' that embeds the HTML table, tr, and th tags 
  in addition to other values (i.e. cells).

- The function firstly renders the header columns up to the number defined by the user(i.e. columns number). After that, 
  it renders the rows up to the number defined by the user.

- Upon finishing the process, '$render_table' variable is returned to the calling page 'ProcessTable'.

- 'ProcessTable' page takes the variable and assign it to the global variable $_SESSION in order to be accessed by 
  'index.php' page.
 
- The control is redirected to the 'index.php' page.

- Finally, 'index.php' page displays the result table on the right div.


#### Execution route 2: Through PHP CLI ####

The execution starts with two options:

##### Option 1: #####


typing 'php index.php'


Here the user is prompted for entering the number of columns.

Then the user is propmted for entering the number of rows.

Both arguments are passed to 'BuikdTable' page for producing the table.

Finally, the table is created and displayed with the headers highlighted.



##### Option 2: #####


typing 'php index.php argument1 argument 2' , where argument 1 is the number of the columns, and argument 2 is the number of the rows.

Arguments can be passed directly after the execution command. 

However, if one argument is passed, the process aborts and displays error message to the user.


### For both options above: ###


- 'BuildTable' page uses 'generateTable' function.

- This function uses ANSI escape sequences in order to control the colour of the table's headers.

- Moreover, 'generateTable' function also builds an array for the header columns and subsequent rows (i.e. an array for each row) in order to be passed to the 'formatTable' function.

- Lastly, 'generateTable' function builds an array of arrays and passes it to the 'formatTable' function.

- 'formatTable' function finds the longest string in the columns and adjust the width across all the cells so that all the cells have an even size.

- 'formatTable' at last returns the rendered table to the calling function 'generateTable' that in turn returns the result table to 'index.php' page.

- Finally, 'index.php' page displays the multiplication table on the command prompt.